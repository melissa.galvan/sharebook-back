<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Book;
use Symfony\Component\HttpFoundation\Request;
use App\Form\BookType;
use JMS\Serializer\SerializerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\BookRepository;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Service\UploadService;

/**
 * @Route("sharebook-api/books", name="books")
 */
class BookController extends AbstractController
{
    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /** Create One **/
    /**
     * @Route("/add", methods="POST")
     */
    public function addBook(Request $request, EntityManagerInterface $managerInterface, UploadService $uploadService)
    {
        $book = new Book();

        $form = $this->createForm(BookType::class, $book);
        $form->submit(json_decode($request->getContent(), true));        

        if ($form->isSubmitted() && $form->isValid()) {
            if ($book->getfileImage()) {
                $book->setImage($uploadService->upload($book->getfileImage()));
            }
            $managerInterface->persist($book);
            $managerInterface->flush();

            return new JsonResponse($this->serializer->serialize($book, 'json'), 201, [], true);
        }

        return $this->json($form->getErrors(true), 400);
    }


    /** Read All **/

    /**
     * @Route(methods="GET")
     */
    public function readAllBooks(BookRepository $bookRepo)
    {
        $books = $bookRepo->findAll();
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/title/asc", methods="GET")
     */
    public function readAllBooksByTitleAsc(BookRepository $bookRepo, Request $request)
    {
        $books = $bookRepo->findByTitleAsc($request->get('title'));
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/title/desc", methods="GET")
     */
    public function readAllBooksByTitleDesc(BookRepository $bookRepo, Request $request)
    {
        $books = $bookRepo->findByTitleDesc($request->get('title'));
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/by-four", methods="GET")
     */
    public function readFourBooks(BookRepository $bookRepo, Request $request)
    {
        $books = $bookRepo->findByFour($request->get('title'));
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/search-title", methods="GET")
     */
    public function searchBooks(BookRepository $bookRepo, Request $request)
    {
        $books = $bookRepo->findByTitle($request->get('search'));
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }

    /** Read One **/

    /**
     * @Route("/{book}", methods="GET")
     */
    public function readOneBook(Book $book)
    {
        return new JsonResponse($this->serializer->serialize($book, 'json'), 200, [], true);
    }

    /** Delete One **/

    /**
     * @Route("/delete/{book}", methods="DELETE")
     */
    public function deleteBookById(Book $book, EntityManagerInterface $managerInterface)
    {
        $managerInterface->remove($book);
        $managerInterface->flush();

        return $this->json('', 204);
    }

    /**
     * @Route("/max-result", methods="GET")
     */
    public function ReadByFive(BookRepository $bookRepo, Request $request)
    {
        $books = $bookRepo->findResults();
        $json = $this->serializer->serialize($books, 'json');

        return new JsonResponse($json, 200, [], true);
    }
}
