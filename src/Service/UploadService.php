<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\File;

/**
 * Ce service nous servira à uploader n'importe quel fichier dans n'importe
 * quel projet
 */
class UploadService {

    /**
     * On lui définit une méthode upload qui renommera le fichier et ira
     * le mettre dans un dossier voulu.
     */
    public function upload(File $file):string {
        //On crée un nom unique de fichier avec la méthode uniqid qu'on
        //hash ensuite en md5 pour uniformiser les noms de fichier (je suppose)
        //et on concatène le nom obtenu avec l'extension du fichier donné
        $name = md5( uniqid() ) . '.' . $file->guessExtension();
        //On va mettre le fichier dans le dossier voulu, qu'on défini
        //dans notre fichier .env (ou .env.local)
        $file->move($_ENV['UPLOAD_DIRECTORY'], $name);
        //On renvoie le nom du fichier généré
        return $name;
    }

}